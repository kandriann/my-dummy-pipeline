# My Dummy Pipeline

## Synopsis
My Dummy Pipeline is a demo project created for the Software Development Process course delivered at University of Milano - Bicocca. The aim of the project is to provide an overview about CI/CD pipelines and involved tools. A dummy Spring Boot application has been created to demonstrate the several steps of a CI/CD pipeline.

## Introduction
[GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/) has been used to implement the example pipelines. It is fully integrated with GitLab and can be used in any repository for free.

The features offered by GitLab CI/CD are many. Even though some of them are not available for free plans, very complex setups can be created with the limited set of features. For this demo project, we tried to give the students an overview about some key aspects, using also some interesting and advanced features.

## How it works
GitLab CI/CD is configured by a file called `.gitlab-ci.yml` placed at the repository's root. The `.gitlab-ci.yml` file defines, using a YAML DSL (Domain Specific Language), the structure and order of the pipelines. It also determines what to execute and what decisions to take when specific conditions are encountered. For example, when a process succeeds or fails.

In this example, we created actually 2 pipelines: the former is executed for the develop branch, the latter for the master branch (See [Pipelines](https://gitlab.com/sw_dev_process_course/my-dummy-pipeline/pipelines)). Each pipeline is composed by several stages which are composed in turn by one or more parallel jobs.

GitLab CI/CD exploits Docker containers for jobs execution, but it is not strictly required to deeply understand how they work for simple scenarios. In any case, the documentation and the reference documentation of GitLab CI/CD (linked in the [Useful resources section](#useful-resources)) provide an exhaustive explanation about Docker interactions.

The [`.gitlab-ci.yml`](.gitlab-ci.yml) in this repository is plenty of comments that can drive the students to better understand each job and the involved features. Thus, no further details about the stages and the jobs are provided in this README.

## Useful resources
Here below, a list of useful resources about GitLab CI/CD. These links, along with the ones in the `.gitlab-ci.yml` file, provide almost all the details and can help the students to accomplish the pipeline creation.

* GitLab CI/CD:
  * [GitLab CI/CD Main Doc](https://docs.gitlab.com/ee/ci/README.html)
  * [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/README.html)
  * [GitLab CI/CD Examples](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-examples)
  * [GitLab CI/CD Docker Integration](https://docs.gitlab.com/ee/ci/docker/README.html)
  * [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
  * [Using SSH keys with GitLab CI/CD](https://docs.gitlab.com/ee/ci/ssh_keys/)
  * [CI/CD essentials from scratch with Gitlab](https://medium.com/faun/ci-cd-essentials-from-scratch-with-gitlab-61502acf318e)
  * [A gitlab-ci config to deploy to your server via ssh](https://medium.com/@hfally/a-gitlab-ci-config-to-deploy-to-your-server-via-ssh-43bf3cf93775)

## Other useful DevOps resources

* Docker:
  * [What is a Container?](https://www.docker.com/resources/what-container)
  * [Docker Playground](https://labs.play-with-docker.com/)
  * [Docker in Docker?](https://itnext.io/docker-in-docker-521958d34efd)
* Kubernetes:
  * [What is Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)
  * [Helm](https://helm.sh/)
  * [Kubernetes Operator Pattern](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/)
  * [Kubernetes vs Docker Swarm — A Comprehensive Comparison](https://hackernoon.com/kubernetes-vs-docker-swarm-a-comprehensive-comparison-73058543771e)
* CI/CD:
  * [Spinnaker Concepts](https://www.spinnaker.io/concepts/#concepts)
  * [What is Jenkins?](https://jenkins.io/doc/#what-is-jenkins)
  * [Travis CI Core Concepts](https://docs.travis-ci.com/user/for-beginners/)
  * [GoCD](https://www.gocd.org/)
  * [Approacches to Application Release Automation](https://devops.com/approaches-to-application-release-automation/)
  * [7 ways to speed up your GitLab CI/CD times](https://blog.sparksuite.com/7-ways-to-speed-up-gitlab-ci-cd-times-29f60aab69f9)
  * [How to Speed Up Your GitLab CI Pipelines for Node Apps by 40%](https://www.addthis.com/blog/2019/05/06/how-to-speed-up-your-gitlab-ci-pipelines-for-node-apps-by-40/)
  * [Our Gitlab CI pipeline for Laravel applications ](https://ohdear.app/blog/our-gitlab-ci-pipeline-for-laravel-applications)
  * [Continuous deployment from GitLab CI to Kubernetes using Docker-in-Docker](https://engineering.facile.it/blog/eng/continuous-deployment-from-gitlab-ci-to-k8s-using-docker-in-docker/)
* Infrastructure-as-Code and Configuration Management:
  * [Introduction to Terraform](https://www.terraform.io/intro/index.html)
  * [Puppet Enterprise](https://puppet.com/products/puppet-enterprise/)
  * [How Ansible works](https://www.ansible.com/overview/how-ansible-works)
* Monitoring and Visualization:
  * [Monitoring the DevOps Toolchain](https://devops.com/monitoring-the-devops-tool-chain/)
  * [What is Prometheus](https://prometheus.io/docs/introduction/overview/#what-is-prometheus)
  * [Elastic Stack](https://www.elastic.co/products/)
  * [Datadog](https://www.datadoghq.com/)
  * [Zabbix](https://www.zabbix.com/)
  * [Grafana](https://grafana.com/grafana/)
  * [Graphite](https://graphiteapp.org/#overview)

## License
This project is licensed under the AGPLv3. See the [LICENSE](LICENSE) file for details.